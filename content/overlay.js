var debug = false;

/**
* sme.sk
*/
var sme = (function() {
	var utils = {};
	/**
	* * get parameter from url (if exists)
	* */
	utils.urlParam = function(name, url) {
		url = (url) ? url : window.location.href;
		name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
		var regexS = "[\\?&]" + name + "=([^&#]*)";
		var regex = new RegExp(regexS);
		var results = regex.exec(url);
		if (results === null) {
			return false;
		} else {
			return results[1];
		}
		return false;
	};
	/**
	* * Article ID
	* */
	utils.articleId = function(doc) {
		var articleId = doc.location.pathname.split('/')[2];
		if (parseInt(articleId, 10) == articleId) {
			return articleId;
		} else {
			return false;
		}
		return false;
	};
	/**
	* * Get video ID
	* */
	utils.videoId = function(doc) {
		var videoId = doc.location.pathname.split('/')[2];
		if (parseInt(videoId, 10) == videoId) {
			return videoId;
		} else {
			return false;
		}
		return false;
	};
	/**
	* * Enable video content
	* */
	var allowVideo = function(doc) {
		try {
			//check for piano content (message)
			var isPiano = ($('.tvpiano',doc).length != 0);
			if (isPiano) {
				//console.log('Nepi Jano: Changing content :) ');
				var articleId = utils.articleId(doc);
				debug && Application.console.log("nepifero:sme.allowVideo:articleID: " + articleId);
				if (articleId) {
					//css3 "magic", blur video window (not supported in Gecko yet)
					//$('.video',doc).attr('style', '-moz-transition: all 1s ease-in-out');
					//$('.video',doc).attr('style', 'filter: blur(8px);');
					//get article id from URL
					var url = 'http://s.sme.sk/export/phone/html/?vf=' + articleId;
					function handleStateChange() {
						if (xhr.readyState === 4) {
							//$('#article-box #itext_content').empty();
							var data = xhr.responseText;
							//remove javascript from response
							data = data.replace(/<script/g, '<!--script');
							data = data.replace(/<\/script/g, '</script--');
							//some magic
							$('.video',doc).html(data);
							$('.video h1',doc).hide();
							$('.video style',doc).remove();
							$('.v-podcast-box',doc).remove();
							var videoSrc = '<video src="' + $($('.video .iosvideo a',doc)[0]).attr('href') + '" controls poster="' + $($('.video .iosvideo img',doc)[0]).attr('src') + '" width="640" height="360">';
							debug && Application.console.log("nepifero:sme.allowVideo:video src : " + videoSrc);
							//$('.video',doc).prepend('<video src="' + $($('.video .iosvideo a',doc)[0]).attr('href') + '" controls poster="' + $($('.video .iosvideo img',doc)[0]).attr('src') + '" width="640" height="360" type="video/mp4">');
							$('.video',doc).prepend(videoSrc);
							//$('.video .tv-video',doc).hide();
							//unblur video window (not supported in Gecko yet
							/*
							var t = setTimeout(function() {
								//$('.video').attr('style', 'filter: blur(0px);');
							}, 500);
							*/
						}
					}
					var xhr = new XMLHttpRequest();
					xhr.onreadystatechange = handleStateChange;
					xhr.open("GET", url, true);
					xhr.send();
				}
			}
		} catch(e) {
			console.error('nepifero:allowVideo:error', e);
		}
	};
	/**
	* * Enable content
	* */
	var allowArticle = function(doc) {
		try {
			//this is not pretty but who cares
			var isPiano1 = ($('#article-box #itext_content .art-perex-piano',doc).length != 0);
			var isPiano2 = ($('#article-box #itext_content .art-nexttext-piano',doc).length != 0);
			debug && Application.console.log("nepifero:sme.allowArticle:Checking if piano article");
			debug && Application.console.log("nepifero:sme.allowArticle:isPiano1: " + isPiano1 + " isPiano2: " + isPiano2);
			if (isPiano1 || isPiano2) {
				debug && Application.console.log("nepifero:sme.allowArticle:Fixing sme.sk piano article");
				//console.log('Nepi Jano: Changing content :) ');
				var articleId = utils.articleId(doc);
				debug && Application.console.log("nepifero:sme.allowArticle:articleID: " + articleId);
				if (articleId) {
					//css3 "magic", blur window (not supported in Gecko yet)
					//$('#article-box #itext_content',doc).attr('style', '-moz-transition: all 1s ease-in-out');
					//$('#article-box #itext_content',doc).attr('style', 'filter: blur(8px);');
					//get article id from URL
					var url = 'http://s.sme.sk/export/phone/html/?cf=' + articleId;
					debug && Application.console.log("nepifero:sme.allowArticle.articleID: url=" + url);
					var xhr = new XMLHttpRequest();
					function handleStateChange() {
						if (xhr.readyState === 4) {
							//$('#article-box #itext_content').empty();
							var data = xhr.responseText;
							//remove javascript from response
							data = data.replace(/<script/g, '<!--script');
							data = data.replace(/<\/script/g, '</script--');
							//some magic
							$('#article-box #itext_content',doc).html(data);
							$('#article-box #itext_content h1',doc).hide();
							$('#article-box #itext_content .discus',doc).hide();
							$('#article-box #itext_content link',doc).remove();
							$('#article-box #itext_content style',doc).remove();
							$('#article-box a',doc).each(function(index) {
								//change s.sme.sk/export/phone/?c=XXX to www.sme.sk/c/XXX/
								var url = $(this).attr('href');
								var cId = utils.urlParam('c', $(this).attr('href'));
								if (/s.sme.sk\//i.test(url) && cId) {
									$(this).attr('href', 'http://www.sme.sk/c/' + cId + '/');
								}
							});
							//unblur window (not supported in Gecko yet
							/*
							var t = setTimeout(function() {
								$('#article-box #itext_content',doc).attr('style', '-moz-filter: blur(0px);');
							}, 500);
							*/
						}
					}
					xhr.onreadystatechange = handleStateChange;
					xhr.open("GET", url, true);
					xhr.send();
				}
			}
		} catch(e) {
			console.error('nepifero:sme.allowArticle:error', e);
		}
	};
	var init = function(doc) {
		debug && Application.console.log("nepifero:sme.init()");
		if (/tv.sme.sk\//i.test(doc.location)) {
			debug && Application.console.log("nepifero:sme.init:tv.sme.sk detected");
			allowVideo(doc);
		} else if (/sme.sk\/c\//i.test(doc.location)) {
			debug && Application.console.log("nepifero:sme.init:sme.sk detected");
			allowArticle(doc);
		}
	};
	return {
		init : init
	}
//};
})();

/**
* etrend.sk
*/
var etrend = (function() {
    var init = function(doc) {
        try {
            //check for piano
            var isPiano = ($('#article_detail .piano-box',doc).length != 0);
            var isPaid = ($('#article_detail .active_box .boxes',doc).length != 0);
            if (isPiano || isPaid) {
                var articleId = false;
                articleId = $($('#article_detail .title div',doc)[0]).text();
                if (articleId) {
                    allowArticle(doc,articleId);
                }

            }
        } catch(e) {
            console.error('nepifero:etrend.init:error', e);
        }
    };
    var allowArticle = function(doc,articleId) {
        function createUUID() {
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 40; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            var uuid = s.join("");
            return uuid;
        }

        //CSS3 magic, not implement in Gecko yet
        //$('#article_text',doc).attr('style', '-moz-transition: all 1s ease-in-out');
        //$('#article_text',doc).attr('style', 'filter: blur(8px);');
        var url = 'http://www.etrend.sk/services/IphoneAppDict.html?deviceType=1&device=' + createUUID() + '&quality=hi&queryType=articleDetail&uid=' + articleId;
        function handleStateChange() {
            if (xhr.readyState === 4) {
                try {
                    var xml = $.parseXML(xhr.responseText);
                    var $xml = $(xml)
                    var article = $xml.find('string');
                    //if exists then replace piano content...
                    if (article.length > 0) {
                        $('#article_text',doc).html($('<div/>').html(article).text());
                    }
                } catch(e) {
                    console.error('nepifero:etrend.allowArticle:error:', e);
                }
                /*
                var t = setTimeout(function() {
                    $('#article_text').attr('style', '-webkit-filter: blur(0px);');
                }, 500);
                */
            }
        }
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = handleStateChange;
        xhr.open("GET", url, true);
        xhr.send();

    }
    return {
        init : init
    }
})();

/**
* hn.sk
*/
var hn = (function () {
    var init = function (doc) {
        try {
            //check for piano
            var isPiano = ($('#body_inhalt .piano-locked-article-top',doc).length != 0);
            if (isPiano) {
                var feed = false;
                //titulka
                if (/hnonline.sk\/c1/i.test(doc.location)) {
                    feed = 'https://apiserver.hnonline.sk/content/fallback/xml/list/titulka/';
                }
                //ekonomika
                if (/hnonline.sk\/ekonomika\//i.test(doc.location)) {
                    feed = 'https://apiserver.hnonline.sk/content/fallback/xml/list/ekonomika-a-firmy/';
                }
                //slovensko
                if (/hnonline.sk\/slovensko\//i.test(doc.location)) {
                    feed = 'https://apiserver.hnonline.sk/content/fallback/xml/list/slovensko/';
                }
                //svet
                if (/hnonline.sk\/svet\//i.test(doc.location)) {
                    feed = 'https://apiserver.hnonline.sk/content/fallback/xml/list/svet/';
                }
                //nazory
                if (/hnonline.sk\/nazory\//i.test(doc.location)) {
                    feed = 'https://apiserver.hnonline.sk/content/fallback/xml/list/nazory-a-analyzy/';
                }
                //sport
                if (/hnonline.sk\/sport\//i.test(doc.location)) {
                    feed = 'https://apiserver.hnonline.sk/content/fallback/xml/list/sport/';
                }
                //get article id from url
                var articleId = false;
                articleId = (('' + doc.location).split('-')[1]);
                if (feed && articleId) {
                    allowArticle(doc,articleId, feed);
                }
            }
        } catch (e) {
            console.error('nepifero:hn.init:error', e);
        }
    };
    var allowArticle = function (doc,articleId, feed) {
        //CSS3 magic, not implement in Gecko yet
        //$('#body_inhalt .detail-text').attr('style', '-webkit-transition: all 1s ease-in-out');
        //$('#body_inhalt .detail-text').attr('style', '-webkit-filter: blur(8px);');
        function handleStateChange() {
            if (xhr.readyState === 4) {
                try {
                    var xml = $.parseXML(xhr.responseText);
                    var $xml = $(xml)
                    //sollution from 2013.05.16 was fixed by hnonline.sk developers
                    /*
var article = $xml.find('article[id="' + articleId + '"] body');
console.log(article)
//if exists then replace piano content...
if (article.length > 0) {
$('#body_inhalt .detail-text').html($('<div/>').html(article).text());
}
*/
                    //sollution from 2013.05.19 was fixed by hnonline.sk developers
                    /*$xml.find('article').each(function() {
var url = $(this).find('url').text();
if (url.split('-')[1] == (''+document.location).split('-')[1]) {
$('#body_inhalt .detail-text').html($('<div/>').html($(this).find('body')).text());
}
});*/
                    //new sollution :)
                    //hint to hnonline.sk developers: use oAuth ;)
                    var titleWeb = $('#body_inhalt h1').text();
                    $xml.find('article').each(function () {
                        var titleXml = $(this).find('title').text();
                        if ($.trim(titleXml) === $.trim(titleWeb)) {
                            $('#body_inhalt .detail-text').html($('<div/>').html($(this).find('body')).text());
                        }
                    })
                } catch (e) {
                    console.error('nepifero:hn.allowArticle:error', e);
                }
                var t = setTimeout(function () {
                    $('#body_inhalt .detail-text').attr('style', '-webkit-filter: blur(0px);');
                }, 500);
            }
        }
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = handleStateChange;
        xhr.open("GET", feed, true);
        xhr.send();
    }
    return {
        init: init
    }
})();

var nepifero = {
	init: function() {
		//Components.utils.reportError("init() called");
		this.initialized=true;
		var appcontent = document.getElementById("appcontent");   // browser
		debug && Application.console.log("nepifero.init");
		if(appcontent){
		        appcontent.addEventListener("DOMContentLoaded", nepifero.onPageLoad, true);
			debug && Application.console.log("nepifero.init:DOM loaded");
		}

	},

	onPageLoad: function(aEvent) {
		var doc = aEvent.originalTarget;
		if (/sme.sk\//i.test(doc.location.href)) {
			debug && Application.console.log("nepifero.onPageLoad:SME.sk detected");
			sme.init(doc);
		}
        //hnonline.sk
        if (/hnonline.sk\//i.test(doc.location)) {
            hn.init(doc);
        }
        //etrend.sk
        if (/etrend.sk\//i.test(doc.location)) {
            etrend.init(doc);
        }
	}
};


window.addEventListener("load", function load(event){
	window.removeEventListener("load", load, false); //remove listener, no longer needed
	nepifero.init();  
},false);
